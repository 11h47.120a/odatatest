sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment"
], function (Controller, ODataModel, MessageToast, MessageBox, Fragment) {
	"use strict";
	var validationCbb = 0,
		validationIdInput = 0,
		validationUserInput = 0,
		validationDP = 0,
		checkAddFragment = 0,
		checkUpdateFragment = 0;

	return Controller.extend("odata.odata.controller.View1", {
		onInit: function () {},

		onAdd: function () {
			var oView = this.getView(),
				that = this;
			if (checkAddFragment == 1)
				this.byId("addDialog").open();
			else {
				if (!this.byId("addDialog")) {
					Fragment.load({
						id: "idFragment",
						name: "odata.odata.fragments.addProduct",
						controller: that
					}).then(function (oDialog) {
						oView.addDependent(oDialog);
						that.dialog = oDialog;
						that.dialog.open();
					});
					checkAddFragment = 1;
				} else {
					this.byId("addDialog").open();
				}
			}
		},

		onSubmit: function () {
			var sID = sap.ui.getCore().byId("idFragment--ip1").getValue(),
				sReleaseDate = sap.ui.getCore().byId("idFragment--DP1").getValue(),
				sPrice = sap.ui.getCore().byId("idFragment--ip3").getValue(),
				sRating = sap.ui.getCore().byId("idFragment--cb1").getValue();

			var oEntry = {};
			oEntry.ID = sID;
			oEntry.Price = sPrice;
			oEntry.ReleaseDate = sReleaseDate;
			oEntry.Rating = sRating;

			var oModel = new ODataModel("/destination/northwind/V2/(S(j2e21wjh4ry31zkkgy4n12xm))/OData/OData.svc/");
			var oTable = this.byId("idProductsTable");
			if (validationCbb && validationIdInput && validationUserInput && validationDP) {
				oModel.create('/Products', oEntry, {
					success: function () {
						MessageToast.show("Create Success");
						oTable.getBinding("items").refresh();
					},
					error: function () {
						MessageToast.show("false");
						oTable.getBinding("items").refresh();
					}
				});
			} else {
				MessageBox.alert("A validation error has occured. Complete your input first");
			}

			oModel.refresh(true);

			this.onCancel();
		},

		onCancel: function () {
			this.dialog.close();
		},

		onEdit: function () {
			var oView = this.getView(),
				that = this;
			if (checkUpdateFragment == 1)
				Fragment.load({
						name: "odata.odata.fragments.updateProduct",
						controller: that
					}).then(function (oDialog) {
						oView.addDependent(oDialog);
						that.dialog = oDialog;
						that.dialog.open();
					});
			else {
				if (!this.byId("updateDialog")) {
					Fragment.load({
						id: "idFragmentUpdate",
						name: "odata.odata.fragments.updateProduct",
						controller: this
					}).then(function (oDialog) {
						oView.addDependent(oDialog);
						that.dialog = oDialog;
						that.dialog.open();
					});
					checkUpdateFragment = 1;
				} else {
					this.byId("updateDialog").open();
				}
			}
		},

		onUpdate: function () {
			var sID = sap.ui.getCore().byId("idFragmentUpdate--cb2").getValue(),
				sReleaseDate = sap.ui.getCore().byId("idFragmentUpdate--DP2").getValue(),
				sPrice = sap.ui.getCore().byId("idFragmentUpdate--ip4").getValue(),
				sRating = sap.ui.getCore().byId("idFragmentUpdate--cb3").getValue();

			var oEntry = {};
			oEntry.ID = sID;
			if (sPrice != '')
				oEntry.Price = sPrice;
			if (sReleaseDate != '')
				oEntry.ReleaseDate = sReleaseDate;
			if (sRating != '')
				oEntry.Rating = sRating;

			var oModel = new ODataModel("/destination/northwind/V2/(S(j2e21wjh4ry31zkkgy4n12xm))/OData/OData.svc/");
			var oTable = this.byId("idProductsTable");
			var temp = '/Products(' + sID + ')';

			if (validationCbb && validationUserInput && validationDP) {
				oModel.update(temp, oEntry, {
					success: function () {
						MessageToast.show("update Success");
						oTable.getBinding("items").refresh();
					},
					error: function () {
						MessageToast.show("false");
					}
				});
			} else {
				MessageBox.alert("A validation error has occured. Complete your input first");
			}

			oModel.refresh(true);

			this.onCancel();
		},

		onDelete: function () {
			var oTable = this.byId("idProductsTable");
			var iIndex = oTable.getSelectedItem();

			var oView = this.getView(),
				that = this;

			if (iIndex === null) {
				if (!this.byId("confirmDialogNoItemSelect")) {
					Fragment.load({
						id: oView.getId(),
						name: "odata.odata.fragments.ConfirmDialogNoItemSelect",
						controller: that
					}).then(function (oDialog) {
						// connect dialog to the root view of this component (models, lifecycle)
						oView.addDependent(oDialog);
						oDialog.open();
					});
				} else {
					this.byId("confirmDialogNoItemSelect").open();
				}
			} else {
				if (!this.byId("confirmDialog")) {
					Fragment.load({
						id: oView.getId(),
						name: "odata.odata.fragments.ConfirmDialog",
						controller: that
					}).then(function (oDialog) {
						// connect dialog to the root view of this component (models, lifecycle)
						oView.addDependent(oDialog);
						oDialog.open();
					});
				} else {
					this.byId("confirmDialog").open();
				}
			}
		},

		onConfirmDialog: function () {
			var oTable = this.byId("idProductsTable");
			var iIndex = oTable.getSelectedItem();
			var sMsg;
			if (iIndex < 0) {
				sMsg = "no item selected";
			} else {
				sMsg = oTable.getSelectedContexts(iIndex);
			}
			var te = sMsg[sMsg.length - 1].sPath;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/destination/northwind/V2/(S(j2e21wjh4ry31zkkgy4n12xm))/OData/OData.svc/");
			oModel.remove(te, {
				success: function () {
					oTable.getBinding("items").refresh();
					MessageToast.show("Success");
				},
				error: function (err) {
					oTable.getBinding("items").refresh();
					MessageToast.show("Fail");
				}
			});
			oModel.setRefreshAfterChange(false);
			this.onCloseDialog();
		},

		onCloseDialog: function () {
			this.byId("confirmDialog").close();
		},
		onCloseDialogNoItemSelect: function () {
			this.byId("confirmDialogNoItemSelect").close();
		},
		handleCreateNewID: function (oEvent) {
			var sUserInput = oEvent.getParameter("value");
			var oInputControl = oEvent.getSource();
			var oModel = new ODataModel("/destination/northwind/V2/(S(j2e21wjh4ry31zkkgy4n12xm))/OData/OData.svc/");
			var temp = '/Products(' + sUserInput + ')';

			if (!sUserInput.match(/^\d+/)) {
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
				oInputControl.setValueStateText("Please enter a valid ID!");
				validationIdInput = 0;
			} else {
				oModel.read(temp, {
					success: function () {
						oInputControl.setValueState(sap.ui.core.ValueState.Error);
						oInputControl.setValueStateText("ID already exist!");
						validationIdInput = 0;
					},
					error: function () {
						oInputControl.setValueState(sap.ui.core.ValueState.Success);
						validationIdInput = 1;
					}
				});
			}
		},
		handleChangeCb: function (oEvent) {
			var oValidatedComboBox = oEvent.getSource(),
				sSelectedKey = oValidatedComboBox.getSelectedKey(),
				sValue = oValidatedComboBox.getValue();

			if (sValue === "" || !sSelectedKey) {
				oValidatedComboBox.setValueState("Error");
				oValidatedComboBox.setValueStateText("Please enter a valid item!");
				validationCbb = 0;
			} else {
				oValidatedComboBox.setValueState("Success");
				validationCbb = 1;
			}
		},
		handleChangeDP: function (oEvent) {
			var oValidatedDatePicker = oEvent.getSource();
			var bValid = oEvent.getParameter("valid");

			if (!bValid) {
				oValidatedDatePicker.setValueState("Error");
				oValidatedDatePicker.setValueStateText("Please enter a valid Date!");
				validationDP = 0;
			} else {
				oValidatedDatePicker.setValueState("Success");
				validationDP = 1;
			}
		},

		handleUserInput: function (oEvent) {
			var sUserInput = oEvent.getParameter("value");
			var oInputControl = oEvent.getSource();
			if (!sUserInput.match(/^\d+/)) {
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
				oInputControl.setValueStateText("Please enter a valid value!");
				validationUserInput = 0;
			} else {
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
				validationUserInput = 1;
			}
		}
	});
});